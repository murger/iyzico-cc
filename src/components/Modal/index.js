import React from 'react';
import { inject, observer } from 'mobx-react';
import './index.scss';

@inject('store')
@observer
class Modal extends React.Component {
    componentWillMount () {
        let eps = this.props.character.episodes;

		this.props.store.getEpisodes(eps);
    }

    closeOnOuterClick (e) {
        let isWindow = this.window.contains(e.target);

        if (!isWindow) {
            this.props.closeHandler();
        }
    }

	render () {
        let c = this.props.character || {},
            eps = this.props.store.episodes;

		return (
            <section
                className="modal"
                onClick={this.closeOnOuterClick.bind(this)}>
                <div
                    className="window"
                    ref={el => (this.window = el)}>
                    <span
                        className="close"
                        onClick={this.props.closeHandler}>&times;</span>
                    <div
                        className="photo"
                        style={{ backgroundImage: `url(${c.image})` }}></div>
                    <div className="name">{c.name}</div>
                    <div className="origin">{c.origin.name}</div>
                    {eps.length > 0 &&
			            <React.Fragment>
                            <ol className="episodes">
                                {eps.map(e => <li key={e.id}>{e.name}</li>)}
                            </ol>
			            </React.Fragment>
                    }
                </div>
            </section>
        );
    }
};

export default Modal;