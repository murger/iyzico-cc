import React from 'react';
import { inject, observer } from 'mobx-react';
import Item from '../Item';
import Modal from '../Modal';
import './index.scss';

@inject('store')
@observer
class List extends React.Component {
	constructor (props) {
		let store = props.store;

		super(props);

		this.state = {
			modal: false,
			character: null
		};

		window.onscroll = () => {
			let height = window.innerHeight,
				scroll = document.documentElement.scrollTop,
				offset = document.documentElement.offsetHeight,
				tolerance = 20;

			if (!store.hasError || !store.isLoading || store.hasMore) {
				if (height + scroll >= offset - tolerance && !store.isFetching) {
					this.loadMore();
				}
			}
		};
	}

	loadMore () {
		this.props.store.getMoreCharacters();
	}

	toggleModal (character) {
		this.setState({
			character,
			modal: !!character
		});
	}

	renderLoader () {
		return <div className="loader" />;
	}

	renderError () {
		return <div className="notice">No data</div>;
	}

	render () {
		let characters = this.props.store.characters,
			modal = (this.state.modal)
				? <Modal
					character={this.state.character}
					closeHandler={this.toggleModal.bind(this, null)} />
				: null;

		if (this.props.store.isLoading) {
			return this.renderLoader();
		} else if (this.props.store.hasError) {
			return this.renderError();
		} else {
			return (
				<React.Fragment>
					{modal}
					<ol className="list">
						{characters.map(c =>
							<Item
								key={c.id}
								image={c.image}
								name={c.name}
								clickHandler={this.toggleModal.bind(this, c)} />
						)}
					</ol>
				</React.Fragment>
			);
		}
	}
};

export default List;
