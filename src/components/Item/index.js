import React from 'react';
import './index.scss';

class Item extends React.Component {
	render () {
		return (
            <li className="item"
                key={this.props.id}
                style={{ backgroundImage: `url(${this.props.image})` }}
                onClick={this.props.clickHandler}>
                <span className="name">{this.props.name}</span>
            </li>
        );
    }
};

export default Item;