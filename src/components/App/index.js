import React from 'react';
import { inject, observer } from 'mobx-react';
import List from '../List';
import './index.scss';

@inject('store')
@observer
class App extends React.Component {
	componentWillMount () {
		this.props.store.getCharacters();
	}

	render () {
		return (
			<React.Fragment>
				<List />
			</React.Fragment>
		);
	}
}

export default App;
