import { observable, action, computed, toJS } from 'mobx';
import services from '../services';

class StateStore {
	@observable episodes = [];
	@observable characters = [];
	@observable isLoading = true;
	@observable isFetching = false;
	@observable hasError = false;
	@observable pages = {
		next: 0,
		total: 0
	};

	@computed get hasMore () {
		return (this.pages.actual < this.pages.total);
	}

	@action setError (state) {
		this.hasError = state;
	}

	@action getCharacters () {
		this.isLoading = true;
		services.rickmorty.getCharacters().then(res => {
			this.isLoading = false;
			this.setError(!res || !res.data);

			if (res.data && res.data.length) {
				this.characters = res.data;
				this.pages.next = res.next;
				this.pages.total = res.pages;
			}
		}).catch(err => {
			console.log(err);
			this.isLoading = false;
			this.setError(true);
		});
	}

	@action getMoreCharacters () {
		if (!this.pages.next) {
			return;
		}

		this.isFetching = true;
		services.rickmorty.getCharacters(this.pages.next).then(res => {
			this.isFetching = false;
			if (res.data && res.data.length) {
				this.characters = res.data;
				this.pages.next = res.next;
			}
		}).catch(err => {
			console.log(err);
			this.isFetching = false;
		});
	}

	@action getEpisodes (set) {
		let limit = 5,
			max = set.length - 1,
			latest = set.slice(max - limit, max);

		this.episodes = [];
		services.rickmorty.getEpisodes(latest).then(data => {
			if (data) {
				this.episodes = data;
			}
		}).catch(err => console.log(err));
	}
}

export default new StateStore();
