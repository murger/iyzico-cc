import CharacterEntity from '../entities/character';
import EpisodeEntity from '../entities/episode';

export default class RickMortyService {
	constructor () {
		this._data = {
			episodes: [],
			characters: []
		};

		this._characters = null;
		this._episodes = null;
		this._host = 'https://rickandmortyapi.com/api';
	}

	getCharacters (page) {
		if (!this._characters) {
			let p = page || 1,
				uri = [this._host, 'character', '?page=' + p].join('/');

			this._characters = fetch(uri, {
				mode: 'cors',
				// cache: 'no-cache'
			})
			.then(res => res.json())
			.then(data => {
				this._characters = null;

				// On error
				if (!data || !data.results) {
					return null;
				}

				// Reset
				if (!page) {
					this._data.characters = [];
				}

				// Prep data
				for (let item of data.results) {
					this._data.characters.push(new CharacterEntity(item));
				}

				// Grab next
				let [u, next] = data.info.next.split('=');

				return {
					data: this._data.characters,
					pages: data.info.pages,
					next: +next
				};
			})
			.catch(() => this._characters = null);
		}

		return this._characters;
	}

	getEpisodes (set) {
		if (!this._episodes) {
			let uri = [this._host, 'episode', set.join(',')].join('/');

			this._episodes = fetch(uri, {
				mode: 'cors',
				// cache: 'no-cache'
			})
			.then(res => res.json())
			.then(data => {
				this._episodes = null;

				// On error
				if (!data || !data.length) {
					return null;
				}

				// Prep data
				this._data.episodes = [];
				for (let item of data) {
					this._data.episodes.push(new EpisodeEntity(item));
				}

				return this._data.episodes;
			})
			.catch(() => this._episodes = null);
		}

		return this._episodes;
	}
}
