import RickMortyService from './rickmorty';

export default {
	rickmorty: new RickMortyService()
};
