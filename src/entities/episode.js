export default class EpisodeEntity {
	constructor (item) {
		this._id = item.id;
		this._name = item.name;
	}

	get id () {
		return this._id;
	}

	get name () {
		return this._name;
	}
}
