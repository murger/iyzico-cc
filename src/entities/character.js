export default class CharacterEntity {
	constructor (item) {
		this._id = item.id;
		this._name = item.name;
		this._image = item.image;
		this._origin = item.origin;
		this._episodes = [];

		for (let e of item.episode) {
			let uri = e.split('/');
			this._episodes.push(uri[uri.length - 1]);
		}
	}

	get id () {
		return this._id;
	}

	get name () {
		return this._name;
	}

	get image () {
		return this._image;
	}

	get origin () {
		return this._origin;
	}

	get episodes () {
		return this._episodes;
	}
}
