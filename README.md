#### iyzico
## Coding Challenge

Please install dependencies first via the command `npm i`.

Then, in order to run it in dev mode, use `npm start` -- this will start Parcel (bundler) in watch mode.

If you'd rather like the compiled code, please use `npm run build` and look
into `dist/` folder.

#### Known issues

1. Challenge utilises a custom setup instead of `create-react-app`, this is due to CRA not yet supporting decorator syntax out of the box.

2. There seems to be a vulnerability (moderate) with a dependency of the bundler, this affects development environment only.
